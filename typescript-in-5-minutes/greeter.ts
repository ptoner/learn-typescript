function greeter(person: Person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}

class Student {
    fullName: string;
    constructor(
        public firstName, 
        public middleInitial,
        public lastName
    ) {

    }
}


interface Person {
    firstName: string;
    lastName: string;
}

// let user = { 
//     firstName: "Jane56",
//     lastName: "User2"
// }

let user = new Student("Pat", "M", "Toner")

console.log(user)


let result = greeter(user)



// console.log(greeter(3))
// console.log(greeter(new Date()))
// console.log(greeter({
//     firstName: "Pat",
//     lastName: "Toner"
// }))

document.body.textContent = result

console.log(result)
