function greeter(person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}
var Student = /** @class */ (function () {
    function Student(firstName, middleInitial, lastName) {
        this.firstName = firstName;
        this.middleInitial = middleInitial;
        this.lastName = lastName;
    }
    return Student;
}());
// let user = { 
//     firstName: "Jane56",
//     lastName: "User2"
// }
var user = new Student("Pat", "M", "Toner");
console.log(user);
var result = greeter(user);
// console.log(greeter(3))
// console.log(greeter(new Date()))
// console.log(greeter({
//     firstName: "Pat",
//     lastName: "Toner"
// }))
document.body.textContent = result;
console.log(result);
